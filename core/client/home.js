Template.home.events({
    'click #confirm': function () {
        let card_number = $('#card_number');
        let exp_month = $('#exp_month');
        let exp_year = $('#exp_year');
        let cvc = $('#cvc');
        let amount = $('#amount');
        Stripe.card.createToken({
            number: card_number.val(),
            exp_month: exp_month.val(),
            exp_year: exp_year.val(),
            cvc: cvc.val()
        }, function (status, token) {
            if (status === 200) {
                Meteor.call('charge_card', token['id'], amount.val());
            }
        })
    }
});