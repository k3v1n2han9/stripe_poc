Meteor.methods({
    charge_card: function (token, amount) {
        Stripe.charges.create({
            currency: 'cad',
            amount: parseInt(parseFloat(amount) * 100),
            source: token
        }, function (err, charges) {
            if (err !== null) {
                console.warn(err);
            }
            else {
                console.log(charges);
            }
        })
    }
});